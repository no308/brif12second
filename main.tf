
############################################################
# Resource group
############################################################ 
resource "azurerm_resource_group" "rg" {
  name     = var.resource_group_name
  location = var.resource_group_location
}
############################################################
# plan
############################################################ 
resource "azurerm_service_plan" "plan" {
  name                = var.app_service_plan_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  os_type             = "Linux" # "Windows", "Linux"
  sku_name            = "S1"
}
############################################################
# web app
############################################################ 

resource "azurerm_linux_web_app" "app" {
    depends_on = [azurerm_resource_group.rg]
  name                = var.app_service_name
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  service_plan_id     = azurerm_service_plan.plan.id

  site_config {
    # dotnet_framework_version = "v4.0" # deprecated
    always_on = false
    application_stack {
      dotnet_version = "6.0" # "v3.0", "v4.0", "5.0", "v6.0"
    }
  }

  app_settings = {
    "SOME_KEY" = "some-value"
  }

  connection_string {
    name  = "Database"
    type  = "SQLServer"
    value = "Server=some-server.mydomain.com;Integrated Security=SSPI"
  }
}
############################################################
#  Storage acount 
############################################################ 

resource "azurerm_storage_account" "storage" {
    depends_on = [azurerm_linux_web_app.app]
  name                     = var.storage_account_name
  resource_group_name      = azurerm_resource_group.rg.name
  location                 = azurerm_resource_group.rg.location
  account_tier             = "Standard"
  account_replication_type = "GRS" # LRS
  allow_blob_public_access = true
}

resource "azurerm_storage_container" "container" {
    depends_on = [azurerm_storage_account.storage]
  name                  = var.storage_container_name
  storage_account_name  = azurerm_storage_account.storage.name
  container_access_type = "container" # "blob" "private"
}

resource "azurerm_storage_blob" "blob" {
        depends_on = [azurerm_storage_container]
  name                   = "sample-file.sh"
  storage_account_name   = azurerm_storage_account.storage.name
  storage_container_name = azurerm_storage_container.container.name
  type                   = "Block"
  source                 = "commands.sh"
}

############################################################
# App insight
############################################################ 


resource "azurerm_application_insights" "application_insights" {
  name                = var.azurerm_application_insights
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  application_type    = "web"

}


############################################################
# az function
############################################################ 
resource "azurerm_linux_function_app" "application" {
  name                        = var.azurerm_linux_function_app_name
  resource_group_name         = var.resource_group_name
  location                    = azurerm_resource_group.rg.location
  service_plan_id             = azurerm_service_plan.plan.id
  storage_account_name        = var.storage_account_name
  storage_account_access_key  = azurerm_storage_account.storage.primary_access_key
  https_only                  = true
  functions_extension_version = "~4"



  site_config {
    application_stack {
      java_version = "11"
    }
  }

  app_settings = {
    "WEBSITE_RUN_FROM_PACKAGE"    = "1"

    // Monitoring with Azure Application Insights
    "APPINSIGHTS_INSTRUMENTATIONKEY" = var.azure_application_insights_instrumentation_key # to check ?

    # These are app specific environment variables
    "SPRING_PROFILES_ACTIVE" = "prod,azure"

    "AZURE_STORAGE_ACCOUNT_NAME"  = var.storage_account_name
    "AZURE_STORAGE_BLOB_ENDPOINT" = var.azure_storage_blob_endpoint # to test not sure about it
    "AZURE_STORAGE_ACCOUNT_KEY"   = var.azure_storage_account_key # to test not sure about it
  }
}
