variable "resource_group_name" {
  type        = string
  default = "Brief12_Group_7"
}

variable "resource_group_location" {
  type        = string
  default = "West Europe"
}

variable "app_service_plan_name" {
  type        = string
  default = "group7plan"
}

variable "app_service_name" {
  type        = string
  default = "group7app"
}


variable "storage_account_name" {
  type        = string
    default = "group7account"
}

variable "storage_container_name" {
  type        = string
default =  "group7container"
}

variable "azurerm_linux_function_app_name" {
    type = string
default = "group7funApp"
}

variable "azure_storage_account_key" {
  type        = string
  description = "The access key of the Azure Storage account"
 # default = "value" //i must test
}

variable "azure_storage_blob_endpoint" {
  type        = string
  description = "The blob endpoint URL of the Azure Storage account"
 # default = "value" //i must test
}

variable "azurerm_application_insights" {
    type = string
    default = "group7_azurerm_application_insights"
  
}
variable "azure_application_insights_instrumentation_key" {
  type        = string
  description = "The Azure Application Insights instrumentation key"
}